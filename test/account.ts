import axios from "axios";

import * as dotenv from "dotenv";

dotenv.config({ path: "./.env.test" });

const authServiceBaseUrl: string = String(process.env.AUTH_SERVICE_BASE_URL);
const userMobilePhone: string = String(process.env.USER_MOBILE_PHONE);
const userPassword: string = String(process.env.USER_PASSWORD);
const partner1MobilePhone: string = String(process.env.PARTNER1_MOBILE_PHONE);
const partner1Password: string = String(process.env.PARTNER1_PASSWORD);

/**
 *
 * @returns
 */
export const userLogin = () => {
  return login(userMobilePhone, userPassword);
};

/**
 *
 * @returns
 */
export const partner1Login = () => {
  return login(partner1MobilePhone, partner1Password);
};

/**
 *
 * @returns
 */
const login = async (mobilePhone: string, password: string) => {
  const response = await axios.request({
    method: "post",
    url: "/account/login/mobilePhone",
    baseURL: authServiceBaseUrl,
    data: {
      mobilePhone,
      password,
    },
  });

  const token = response?.data["X-Access-Token"];

  return { token };
};
