import express from "express";
import {
  createTodoSchema,
  deleteTodoSchema,
  getTodoSchema,
  updateTodoSchema,
} from "../controller/todo/todo.schema";
import validateResource from "../middleware/validateResource";
import {
  createTodoHandler,
  deleteTodoHandler,
  getListTodoHandler,
  getTodoHandler,
  updateTodoHandler,
} from "../controller/todo/todo.controller";

const router = express.Router();

router
  .route("/")
  .get(getListTodoHandler)
  .post(validateResource(createTodoSchema), createTodoHandler);

router
  .route("/:id")
  .get(validateResource(getTodoSchema), getTodoHandler)
  .patch(validateResource(updateTodoSchema), updateTodoHandler)
  .delete(validateResource(deleteTodoSchema), deleteTodoHandler);

export default router;
